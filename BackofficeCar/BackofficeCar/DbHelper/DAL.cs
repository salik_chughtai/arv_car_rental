﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using AutoMapper;
using BackofficeCar.Models;
using BackofficeRent.Model;
using IdentitySample.Models;
using Microsoft.Ajax.Utilities;

namespace BackofficeCar.DbHelper
{
    //TODO: Repository pattern
    public partial class DAL : IDAL
    {
        public int AddVehicle(VehicleDetailsDto model)
        {
            var vehicle = new Vehicle();
            using (var db = new ApplicationDbContext())
            {
                switch (vehicle.VehicleType)
                {
                    case VehicleType.Car:
                        vehicle = AddCar(model, db);
                        break;
                    case VehicleType.Motorbike:
                        vehicle = AddMotorBike(model, db);
                        break;
                    default:
                        return 0;
                }
                
                db.Vehicles.Add(vehicle);

                var location =  db.Location
                    .SingleOrDefault(i => i.ID == model.LocationID);
                if (location != null)
                {
                    if (location.Vehicles == null) location.Vehicles = new List<Vehicle>();
                    location.Vehicles.Add(vehicle);
                }

                db.SaveChanges();
                return vehicle.ID;
            }
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="model"></param>
        /// <param name="addNewIfNotExists">Not is used</param>
        /// <returns></returns>
        public ResponseMessage UpdateVehicle(int vehicleId, VehicleDetailsDto model, bool addNewIfNotExists = false)
        {
            if (vehicleId <= 0) return null;

            using (var db = new ApplicationDbContext())
            {
                Vehicle vehicle = null;
                vehicle = db.Vehicles.SingleOrDefault(v => v.ID == vehicleId);
                if (vehicle == null) return new ResponseMessage(true, "No vehicle found.");

                vehicle.VehicleType = model.VehicleType;
                vehicle.Name = model.Name;
                vehicle.PricePerDay = model.PricePerDay;
                vehicle.PricePerHour = model.PricePerHour;
                //vehicle.ImageUrl = model.ImageUrl;//TODO:
                vehicle.IsActive = model.IsActive;
                vehicle.UpdatedDate = DateTime.Now;

                //More fields specifically to Type. Todo: is it good appraoch?
                if (vehicle.GetType() == typeof(Car))
                {
                    ((Car)vehicle).Make = model.Make;
                    ((Car)vehicle).RegistrationNumber = model.RegistrationNumber;
                    ((Car)vehicle).CarColor = model.CarColor;
                    ((Car)vehicle).InsuranceNumber = model.InsuranceNumber;
                    ((Car)vehicle).Transmission = model.Transmission;
                    ((Car)vehicle).Classification = model.Classification;
                }
                else if (vehicle.GetType() == typeof(Motorbike))
                {
                    throw new Exception("Not implemented");
                }
                db.SaveChanges();
                var obj =  GetVehicleById(vehicle.ID);
                return new ResponseMessage(obj: obj);
            }
        }
        /// <summary>
        /// Get vehicle By ID
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public VehicleDetailsDto GetVehicleById(int vehicleId)
        {
            using (var db = new ApplicationDbContext())
            {
                var vehicle = db.Vehicles.SingleOrDefault(v => v.ID == vehicleId);
                if (vehicle == null) return null;

                if (vehicle.VehicleType == VehicleType.Car)
                {
                    return new VehicleDetailsDto()
                    {
                        Make = ((Car)vehicle).Make,
                        Name = ((Car)vehicle).Name,
                        CarColor = ((Car)vehicle).CarColor,
                        Classification = ((Car)vehicle).Classification,
                        Transmission = ((Car)vehicle).Transmission,
                        ModelMonth = ((Car)vehicle).ModelMonth,
                        ModelYear = ((Car)vehicle).ModelYear,
                        InsuranceNumber = ((Car)vehicle).InsuranceNumber,
                        RegistrationNumber = ((Car)vehicle).RegistrationNumber,
                        VehicleID = vehicleId,
                        VehicleType = vehicle.VehicleType,
                        PricePerHour = vehicle.PricePerHour,
                        PricePerDay = vehicle.PricePerDay,
                        IsActive = vehicle.IsActive
                    };
                }
                else
                {
                    throw new Exception("Can't fetch Motorbike.");
                }

            }
        }
        /// <summary>
        /// Search Vehicles
        /// </summary>
        /// <param name="vehicleType"></param>
        /// <param name="name"></param>
        /// <param name="make"></param>
        /// <param name="transmission"></param>
        /// <param name="pricePerDay"></param>
        /// <returns></returns>
        public List<VehicleDto> SearchVehicles(IEnumerable<VehicleType> vehicleTypes, string name = null, string make = null,
            VehicleTransmission transmission = VehicleTransmission.All, int? areaId = null)
        {
            using (var db = new ApplicationDbContext())
            {
                var types = new List<Type>();
                foreach (var vehicleType in vehicleTypes)
                {
                    switch (vehicleType)
                    {
                        case VehicleType.Car:
                            types.Add(typeof(Car));
                            break;
                        case VehicleType.Motorbike:
                            types.Add(typeof(Motorbike));
                            break;
                    }
                }

                var list = db.Vehicles.OfType(types).ToList();
                var vehicles = new List<VehicleDto>();
                
                //todo:autoMapper - Transform Model to DTO
                foreach (var vehicle in list)
                {
                    var v = new VehicleDto()
                    {
                        VehicleID = vehicle.ID,
                        VehicleType = vehicle.VehicleType,
                        Name = vehicle.Name,
                        PricePerHour = vehicle.PricePerHour,
                        PricePerDay = vehicle.PricePerDay,
                        Make = vehicle.Make,
                        ImageUrl = vehicle.ImageUrl

                    };
                    switch (vehicle.VehicleType)
                    {
                        case VehicleType.Car:
                            v.CarColor = ((Car) vehicle).CarColor;
                            v.Classification = ((Car) vehicle).Classification;
                            v.Transmission = ((Car) vehicle).Transmission;
                            v.RegistrationNumber = ((Car) vehicle).RegistrationNumber;
                            v.ModelMonth = ((Car)vehicle).ModelMonth;
                            v.ModelYear = ((Car) vehicle).ModelYear;
                            v.InsuranceNumber = ((Car) vehicle).InsuranceNumber;
                            break;
                        case VehicleType.Motorbike:
                            //todo
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    vehicles.Add(v);
                }
                return vehicles;
            }
        }

        public void AddLocation(LocationDto location, int? areaId, string areaName = null)
        {
            using (var db = new ApplicationDbContext())
            {
                var area = db.Area.SingleOrDefault(i => i.ID == areaId || i.Name.ToLower() == areaName.ToLower());
                if (area == null)
                {
                    if (string.IsNullOrEmpty(areaName)) throw new Exception("Area name is empty, New area cannot be created");

                    area = new Area(){ Name = areaName, };
                    db.Area.Add(area);
                }

                db.Location.Add(new Location()
                {
                    Address = location.Address,
                    Area = area
                });
                db.SaveChanges();
            }
        }
        public Location GetLocation(int locationId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Location
                    .SingleOrDefault(i => i.ID == locationId);
            }
        }

        public List<LocationDto> GetLocations(int? areaId)
        {
            using (var db = new ApplicationDbContext())
            {
                var query = (from a in db.Location
                    where !areaId.HasValue || a.Area.ID == areaId.Value
                    select new LocationDto()
                    {
                        LocationId = a.ID,
                        Address = a.Address,
                        AreaName = a.Area.Name,
                        AreaId = a.Area.ID,
                    }).ToList();
                return query.ToList();
            }
        }

        public List<AreaDto> GetAllArea()
        {
            using (var db = new ApplicationDbContext())
            {
                return (from a in db.Area
                        select new AreaDto()
                        {
                            AreaName = a.Name,
                            AreaId = a.ID,
                        }).ToList();
            }
        }

        public List<VehicleDetailsDto> GetAllVehicles()
        {
            using (var db = new ApplicationDbContext())
            {
                var list = (from vehicle in db.Vehicles
                    where vehicle.IsActive
                    select vehicle).ToList();

                var vehicles = new List<VehicleDetailsDto>();
                foreach (var vehicle in list)
                {
                    var v = new VehicleDetailsDto()
                    {
                        VehicleID = vehicle.ID,
                        VehicleType = vehicle.VehicleType,
                        Name = vehicle.Name,
                        PricePerHour = vehicle.PricePerHour,
                        PricePerDay = vehicle.PricePerDay,
                        Make = vehicle.Make,
                        ImageUrl = vehicle.ImageUrl,
                        IsActive = vehicle.IsActive,

                    };
                    switch (vehicle.VehicleType)
                    {
                        case VehicleType.Car:
                            v.CarColor = ((Car) vehicle).CarColor;
                            v.Classification = ((Car) vehicle).Classification;
                            v.Transmission = ((Car) vehicle).Transmission;
                            v.RegistrationNumber = ((Car) vehicle).RegistrationNumber;
                            v.ModelMonth = ((Car) vehicle).ModelMonth;
                            v.ModelYear = ((Car) vehicle).ModelYear;
                            v.InsuranceNumber = ((Car) vehicle).InsuranceNumber;
                            break;
                        case VehicleType.Motorbike:
                            //todo
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    vehicles.Add(v);
                }
                return vehicles;
            }
        }

        #region private

        private Motorbike AddMotorBike(VehicleDto vehicle, ApplicationDbContext db)
        {
            throw new NotImplementedException();
        }

        private Car AddCar(VehicleDetailsDto vehicle, ApplicationDbContext db)
        {
            return new Car()
            {
                CarColor = vehicle.CarColor, 
                Classification = vehicle.Classification, 
                InsuranceNumber = vehicle.InsuranceNumber, 
                Name = vehicle.Name, 
                Make = vehicle.Make, 
                ModelMonth = vehicle.ModelMonth, 
                ModelYear = vehicle.ModelYear, 
                PricePerDay = vehicle.PricePerDay, 
                PricePerHour = vehicle.PricePerHour, 
                RegistrationNumber = vehicle.RegistrationNumber, 
                Transmission = vehicle.Transmission, 
                VehicleType = vehicle.VehicleType,
                ImageUrl = vehicle.ImageUrl, 
                IsActive = vehicle.IsActive, 
                EnteredDate = DateTime.Now, 
                UpdatedDate = DateTime.Now
            };
        }

        #endregion
    }

    public interface IDAL
    {
        int AddVehicle(VehicleDetailsDto model);
        ResponseMessage UpdateVehicle(int vehicleId, VehicleDetailsDto vehicle, bool addNewIfNotExists = false);
        VehicleDetailsDto GetVehicleById(int vehicleId);
        List<VehicleDto> SearchVehicles(IEnumerable<VehicleType> vehicleType, string name = null, string make = null, VehicleTransmission transmission = VehicleTransmission.All, int? areaId = null);
        void AddLocation(LocationDto location, int? areaId, string areaName =null);
        Location GetLocation(int locationId);
        List<LocationDto> GetLocations(int? areaId);
        List<AreaDto> GetAllArea();
        List<VehicleDetailsDto> GetAllVehicles();
    }
}