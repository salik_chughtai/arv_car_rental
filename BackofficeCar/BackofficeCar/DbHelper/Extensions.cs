﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BackofficeCar.DbHelper
{
    public static class Extensions
    {
        public static IQueryable<T> OfType<T>(this IQueryable<T> source, IEnumerable<Type> types)
        {
            var parameter = Expression.Parameter(typeof(T), "a");
            var body = types
                .Select(type => Expression.TypeIs(parameter, type))
                .Aggregate<Expression>(Expression.OrElse);
            var predicate = Expression.Lambda<Func<T, bool>>(body, parameter);
            return source.Where(predicate);

        }

        public static object GetVRentWebResponse(bool isError, string message = null, dynamic data = null)
        {
            return new
            {
                isError= isError,
                message = message,
                data = data
            };
        }
    }
}