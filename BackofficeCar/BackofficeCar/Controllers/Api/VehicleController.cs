﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Services.Description;
using BackofficeCar.AppResource;
using BackofficeCar.DbHelper;
using BackofficeRent.Model;

namespace BackofficeCar.Controllers.Api
{
    public class VehicleController : ApiController
    {
        //todo: add dependency injection
        private static readonly IDAL DAL = new DAL();

        [System.Web.Http.HttpGet]
        [System.Web.Http.AllowAnonymous]
        public IHttpActionResult Search([FromUri]VehicleType[] vehicleTypes = null, string name = null, string make = null,
            VehicleTransmission transmission = VehicleTransmission.All, int? areaId = null)
        {
            if (vehicleTypes == null || !vehicleTypes.Any())
                return Ok(Extensions.GetVRentWebResponse(false,
                    message: MessageStrings.Error_NoVehicleTypeFound));

            var list = DAL.SearchVehicles(vehicleTypes, name, make, transmission, areaId);
            /*return this.Request.CreateResponse(HttpStatusCode.Found, Extensions.GetVRentWebResponse(false, 
                message: (list!=null && list.Any() ? string.Empty : MessageStrings.Message_NoDataFound),
                    data: list));*/
            return Ok(Extensions.GetVRentWebResponse(false,
                message: (list != null && list.Any() ? string.Empty : MessageStrings.Message_NoDataFound),
                data: list));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.AllowAnonymous]
        public IHttpActionResult GetAreas()
        {
            var list = DAL.GetAllArea();
            /*return this.Request.CreateResponse(HttpStatusCode.Found, Extensions.GetVRentWebResponse(false,
                message: (list != null && list.Any() ? string.Empty : MessageStrings.Message_NoDataFound),
                    data: list));*/
            return Ok(Extensions.GetVRentWebResponse(false,
                    message: (list != null && list.Any() ? string.Empty : MessageStrings.Message_NoDataFound),
                    data: list));
        }

    }
}
