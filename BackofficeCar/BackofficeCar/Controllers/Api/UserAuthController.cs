﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackofficeRent.Model;
using IdentitySample.Models;
using Microsoft.AspNet.Identity.Owin;

namespace BackofficeCar.Controllers.Api
{
    public class UserAuthController : ApiController
    {
        private ApplicationSignInManager _signInManager;        public ApplicationSignInManager SignInManager        {            get            {                return _signInManager ??                     Request.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        #region public ActionResults        [HttpPost]        [AllowAnonymous]        public IHttpActionResult Signin(UserAuthenticationDto authentication)        {            var strResult = "";
            var paramUserName = authentication.UserName;
            var paramPassword = authentication.Password;
            var result = SignInManager.PasswordSignIn(
                paramUserName, paramPassword, false, shouldLockout: false);
            switch (result)
            {                case SignInStatus.Success:                    strResult = "Success";                    break;                case SignInStatus.LockedOut:                    strResult = "LockedOut";                    break;                case SignInStatus.RequiresVerification:                    strResult = "RequiresVerification";                    break;                case SignInStatus.Failure:                default:                    strResult = "Failure";                    break;            }            return Ok(strResult);        }        #endregion
    }
}
