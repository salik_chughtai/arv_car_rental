﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackofficeCar.DbHelper;
using BackofficeRent.Model;

namespace BackofficeCar.Controllers
{
    public class ManageVehiclesController : Controller
    {
        //
        // GET: /ManageVehicles/

        public ActionResult Index()
        {
            var DAL = new DAL();
            var list = DAL.GetAllVehicles();
            return View(list);
        }
        public ActionResult Edit(int id)
        {
            if(id<=0) throw  new Exception("Invalid id, please pass valid vehicle id.");
            var DAL = new DAL();
            var obj = DAL.GetVehicleById(id);
            if (obj == null) throw new Exception("No vehicle found");

            return View("Details", obj);
        }
        /// <summary>
        /// TODO: Image Upload
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(VehicleDetailsDto model)
        {
            if (model== null || (model.VehicleID ?? 0) <= 0) throw new Exception("Bad format data passed.");
            
            var DAL = new DAL();
            var obj = DAL.UpdateVehicle(model.VehicleID.Value, model);
            if (obj == null || obj.IsError) throw new Exception(obj == null ? "Null exception occurred" : obj.Message?? "");

            return View("Details", (VehicleDetailsDto)obj.ResponseObject);
        }
        public ActionResult Delete(int id)
        {
            var DAL = new DAL();
            var list = DAL.GetAllVehicles();
            return View("Details", list);
        }
        public ActionResult Create()
        {
            VehicleDetailsDto model = new VehicleDetailsDto();
            model.IsInCreateRecordMode = true;
            model.IsActive = true;
            return View("Details", model);
        }
        [HttpPost]
        public ActionResult Create(VehicleDetailsDto model)
        {
            if (model == null || string.IsNullOrEmpty(model.Name) || string.IsNullOrEmpty(model.RegistrationNumber)) throw new Exception("Bad format data passed.");

            model.IsInCreateRecordMode= true;
            var DAL = new DAL();
            var id = DAL.AddVehicle(model);
            
            if (id > 0) model = new VehicleDetailsDto(){IsInCreateRecordMode = true};

            ViewBag.Message = "Error";
            return View("Details", model);
        }
    }
}
