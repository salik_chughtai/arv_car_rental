namespace BackofficeCar.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ll : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Locations", "Area_ID", c => c.Int());
            CreateIndex("dbo.Locations", "Area_ID");
            AddForeignKey("dbo.Locations", "Area_ID", "dbo.Areas", "ID");
            DropColumn("dbo.Locations", "AreaName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Locations", "AreaName", c => c.String());
            DropForeignKey("dbo.Locations", "Area_ID", "dbo.Areas");
            DropIndex("dbo.Locations", new[] { "Area_ID" });
            DropColumn("dbo.Locations", "Area_ID");
            DropTable("dbo.Areas");
        }
    }
}
