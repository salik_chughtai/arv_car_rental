namespace BackofficeCar.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isActivefieldaddedinlocationandvehicle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Areas", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Vehicles", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Vehicles", "EnteredDate", c => c.DateTime());
            AddColumn("dbo.Vehicles", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.Locations", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "IsActive");
            DropColumn("dbo.Vehicles", "UpdatedDate");
            DropColumn("dbo.Vehicles", "EnteredDate");
            DropColumn("dbo.Vehicles", "IsActive");
            DropColumn("dbo.Areas", "IsActive");
        }
    }
}
