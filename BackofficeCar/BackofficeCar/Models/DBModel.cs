﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using BackofficeRent.Model;

namespace BackofficeCar.Models
{
    public class Vehicle
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public VehicleType VehicleType { get; set; }

        public int PricePerHour { get; set; }
        public int PricePerDay { get; set; }
        //public Location Location { get; set; }
        [MaxLength(20)]
        public string Make { get; set; }
        [MaxLength(255)]
        public string ImageUrl { get; set; }

        public bool IsActive { get; set; }
        public DateTime? EnteredDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class Car : Vehicle
    {
        public CarType Classification { get; set; }
        [MaxLength(15)]
        public string Transmission { get; set; }
        [MaxLength(15)]
        public string RegistrationNumber { get; set; }
        [MaxLength(15)]
        public string CarColor { get; set; }

        public int ModelMonth { get; set; }
        public int ModelYear { get; set; }
        [MaxLength(15)]
        public string InsuranceNumber { get; set; }
    }

    public class Location
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public Area Area { get; set; }
        public string Address { get; set; }
        public ICollection<Vehicle> Vehicles { get; set; }
        public bool IsActive { get; set; }
    }

    public class Area
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required, MaxLength(30)]
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }

    public class Motorbike : Vehicle
    {
    }
}