﻿using System;
using System.Collections.Generic;
using BackofficeCar.DbHelper;
using BackofficeRent.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BackofficeCarTest
{
    [TestClass]
    public class VehicleTest
    {
        IDAL DAL = new DAL();
        [TestMethod]
        public void AddCar()
        {
      
            var obj = new VehicleDetailsDto()
            {
                VehicleType = VehicleType.Car,
                CarColor = "Grey",
                Classification = CarType.HatchBack,
                InsuranceNumber = "",
                Make = "Wira",
                Name = "Proton",
                RegistrationNumber = "MCT122",
                ModelMonth = 1,
                ModelYear = 2016,
                Transmission = "A",
                PricePerHour = 8,
                PricePerDay = 100,
                ImageUrl = "https://s3.caradvice.com.au/wp-content/uploads/2015/07/volkswagen-golf.jpg",
                LocationID = 2
            };
            var id = DAL.AddVehicle(obj);
            Assert.IsTrue(id > 0, "Vehicle ID: " + id);
        }
        [TestMethod]
        public void UpdateVehicle()
        {
            var vehicleId = 3;
            var vehicle = DAL.GetVehicleById(vehicleId);
            if(vehicle ==null) throw  new Exception("No vehicle found against "+vehicleId);
            vehicle.InsuranceNumber = "Test123";
            vehicle.Name = "City";
            vehicle.Make = "Honda";
            vehicle.ImageUrl = "https://s3.caradvice.com.au/wp-content/uploads/2015/07/volkswagen-golf.jpg";

            var obj = DAL.UpdateVehicle(vehicleId, vehicle);
            Assert.IsNotNull(obj);
        }

        [TestMethod]
        public void GetVehicle()
        {
            int vehicleId = 6;
            var vehicle = DAL.GetVehicleById(vehicleId);
            Assert.AreEqual(vehicle.VehicleID, 6);
        }

        [TestMethod]
        public void SearchAllCars()
        {
            var vehicleType = new List<VehicleType>() {VehicleType.Car};
            var list = DAL.SearchVehicles(vehicleType);
            Assert.IsNotNull(list);
        }
        [TestMethod]
        public void SearchAllVehicles()
        {
            var vehicleType = new List<VehicleType>() { VehicleType.Car, VehicleType.Motorbike };
            var list = DAL.SearchVehicles(vehicleType);
            Assert.IsNotNull(list);
        }
        [TestMethod]
        public void SearchAllMotorbikes()
        {
            var vehicleType = new List<VehicleType>() {VehicleType.Motorbike };
            var list = DAL.SearchVehicles(vehicleType);
            Assert.IsNotNull(list);
        }
    }
}
