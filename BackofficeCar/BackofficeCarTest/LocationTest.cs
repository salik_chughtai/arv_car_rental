﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackofficeCar.DbHelper;
using BackofficeRent.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BackofficeCarTest
{
    [TestClass]
    public class LocationTest
    {
        IDAL DAL = new DAL();
        [TestMethod]
        public void AddLocationWithNewArea()
        {
            var obj = new LocationDto() { Address = "Putra jaya rent a car address" };
            var areaName = "Putrajaya";
            DAL.AddLocation(obj, null, areaName);
        }
        [TestMethod]
        public void GetLocation()
        {
            var location = DAL.GetLocation(1);
            Assert.IsNotNull(location);
        }
    }
}
