﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BackofficeRent.Model
{
    public class VehicleDto
    {
        public int? VehicleID { get; set; }
        public string Name { get; set; }
        public VehicleType VehicleType { get; set; }
        public string VehicleTypeDescription {
            get { return VehicleType.DescriptionAttr(); }
        }
        public int PricePerHour { get; set; }
        public int PricePerDay { get; set; }
        public string Make { get; set; }
        public CarType Classification { get; set; }
        public string ClassificationDescription {
            get { return Classification.DescriptionAttr(); }
        }
        public string Transmission { get; set; }
        public string RegistrationNumber { get; set; }
        public string CarColor { get; set; }
        public int ModelMonth { get; set; }
        public int ModelYear { get; set; }
        public string InsuranceNumber { get; set; }
        public string ImageUrl { get; set; }
        public LocationDto Location { get; set; }
        public int LocationID { get; set; }
    }

    public class VehicleDetailsDto : VehicleDto
    {
        [Display(Name = "Vehicle active")]
        public bool IsActive { get; set; }

        public bool IsInCreateRecordMode { get; set; }

        public IList<SelectListItem> VehicleTransmissions
        {
            get
            {
                var list = Enum.GetValues(typeof (VehicleTransmission)).Cast<VehicleTransmission>();
                return (from i in list
                        where i != VehicleTransmission.All
                        select new SelectListItem() 
                        {
                            Text = i.DescriptionAttr(),
                            Value = i.ToString()
                        }).ToArray();
            }
        }
        public IList<SelectListItem> VehicleClassifications
        {
            get
            {
                var list = Enum.GetValues(typeof(CarType)).Cast<CarType>();
                return (from i in list
                        select new SelectListItem()
                        {
                            Text = i.DescriptionAttr(),
                            Value = ((int)i).ToString()
                        }).ToArray();
            }
        }
    }
}
