﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackofficeRent.Model
{
    public class LocationDto
    {
        public int LocationId { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public string Address { get; set; }
    }

    public class AreaDto
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
    }
}
