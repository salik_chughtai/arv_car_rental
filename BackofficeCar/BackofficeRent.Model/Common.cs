﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackofficeRent.Model
{
    public static class Common
    {
        public static string DescriptionAttr<T>(this T source)
        {
            var field = source.GetType().GetField(source.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(
                typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : source.ToString();
        }
    }

    public class ResponseMessage
    {
        public ResponseMessage(object obj, bool isError = false, string message = "success")
        {
            IsError = isError;
            Message = message;
            ResponseObject = obj;
        }
        public ResponseMessage(bool isError, string message)
        {
            IsError = isError;
            Message = message;
        }
        public object ResponseObject { get; set; }
        public string Message { get; set; }
        public bool IsError { get; set; }
    }
    public class DataValueModel
    {
        public string Data { get; set; }
        public int Value { get; set; }
    }
}
