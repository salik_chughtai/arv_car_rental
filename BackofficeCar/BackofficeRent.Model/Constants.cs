﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackofficeRent.Model
{
    public enum CarType
    {
        [Description("Sedan")]
        Sedan,
        [Description("HatchBack")]
        HatchBack,
        [Description("SUV")]
        SUV,
        [Description("MPV")]
        MPV,
        [Description("CrossOver")]
        CrossOver,
        [Description("Others")]
        Others
    }
    public enum VehicleType
    {
        Car,
        Motorbike
    }

    public enum VehicleTransmission
    {
        [Description("Automatic")]
        A,
        [Description("Manual")]
        M,
        [Description("Include All")]
        All
    }
}
