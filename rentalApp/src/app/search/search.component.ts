import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../Services/user-service.service'
import {AppServiceService} from '../Services/app-service.service'

import {AppSettings} from '../model/AppSettings'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  private transmissions:any =[{value:"", name: "All"}, {value: "manual", name:"Manual"}, {value:"auto", name:"Auto"}];
  private areaList:any;
  private searchListing:any;

  private isDataLoaded: boolean = false;
  private isAreaLoded:boolean = false;
  private isSearchResultLoaded:boolean = false;

  private selectedTransmission:string="";
  private selectedAreaName:string="";

  constructor(private userService: UserServiceService, private appService: AppServiceService) { }

  ngOnInit() {
    this.loadAreas();
  }

  onSearchSubmit(frm){
    console.log(frm);
    this.loadSearchResults(AppSettings.VEHICLE_TYPES, this.selectedTransmission, this.selectedAreaName);
  }

  /************ PRIVATE METHODS ************/
  loadAreas(){
    this.appService.getAreas().subscribe(response => {
      console.log(response);
      this.areaList = response.data;
      this.isAreaLoded = true;
      this.isDataLoaded = this.isAreaLoded; //&& this.isSearchResultLoaded;
    });
  }
  loadSearchResults(sVehicleTypes, sTransmission, sSelectedArea){
    this.appService.getSearch(sVehicleTypes, sTransmission, sSelectedArea).subscribe(response => {
      console.log(response);
      this.searchListing = response.data;
      this.isSearchResultLoaded = true;
      this.isDataLoaded = this.isAreaLoded && this.isSearchResultLoaded;
    });
  }
}
