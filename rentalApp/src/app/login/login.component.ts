import { Component, OnInit } from '@angular/core';

import {UserServiceService} from '../Services/user-service.service'
import {IAuthentication} from '../model/Authentication'
import { compileDirectiveFromMetadata } from '@angular/compiler';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private svc: UserServiceService, private router: Router) { }
  private errorDescription:string;

  ngOnInit() {
  }
  
  onLoginSubmit(frm){
    //console.log(frm.txtEmailAddress);
    //console.log(frm.txtPassword);
    this.errorDescription = null;
    let userAuth: IAuthentication = {
      UserName: frm.txtEmailAddress,
      Password: frm.txtPassword
    };

    this.svc.signin(userAuth).subscribe(data => {
      //console.log(data);
      //todo:save to local storage
      console.log(data.access_token);
      this.router.navigate(['']);
    },
    err => {
      if(err.status){
        this.errorDescription = err.error.error_description;
      }
      console.warn(err);
    });
  }
}
