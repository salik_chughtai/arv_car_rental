import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';


import {AppSettings} from '../model/AppSettings'

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  private readonly _baseUrl:string  = AppSettings.API_ENDPOINT;

  constructor(private http : HttpClient) { }

  getAreas(): Observable<any> {
    var url = this._baseUrl + '/vehicle/GetAreas';
      const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    
      return (this.http.get(url, {headers:headers}));
  }
  
  getSearch(vehicleTypes:any, transmission:string, areaId:string): Observable<any> {
    var url = this._baseUrl + '/vehicle/search' 
    + "?vehicleTypes="+vehicleTypes;

    if(transmission) url += "&transmission="+transmission;
    
    if(areaId) url += "&areaId="+areaId;

      const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    
      return (this.http.get(url, {headers:headers}));
  }
}
