import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import {AppSettings} from '../model/AppSettings'

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  private readonly _baseUrl:string = AppSettings.API_ENDPOINT;

  constructor(private http : HttpClient) { }

  signin(userAuth): Observable<any> {
    // var url = this._baseUrl + '/userauth/signin';
    //   const headers = new HttpHeaders()
    //   .set('Content-Type', 'application/json');
    //   return (this.http.post(url, JSON.stringify(userAuth),  {headers:headers}));
    var url =  this._baseUrl +'/token';
      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');
      var data = "username=" + userAuth.UserName + "&password=" + userAuth.Password + "&grant_type=password";
      return (this.http.post(url, data,  {headers:headers}));
  }
}
