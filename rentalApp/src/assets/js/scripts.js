$(document).ready(function(){
    $('.input-daterange').datepicker({
        format: "dd/mm/yyyy",
        startDate: new Date(),
        autoclose: true
    });
}); 